FROM centos
MAINTAINER NEIL <neil@gmail.com>
ADD index.html /var/www/html/index.html
RUN yum update
RUN yum repolist
RUN yum install httpd* -y
EXPOSE 80
CMD systemctl start httpd; tail -f /dev/null

